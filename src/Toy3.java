import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import twitter4j.GeoLocation;
import twitter4j.Place;
import twitter4j.RateLimitStatus;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

public class Toy3 {

	public static void main(String[] args) {

		//initiate twitters
		Twitter[] twitters = new Twitter[Helper.KEYS.length];
		ConfigurationBuilder[] cb = new ConfigurationBuilder[Helper.KEYS.length];
		int currentTwitterIndex=0;
		for(int i=0;i<cb.length;i++){
			cb[i] = new ConfigurationBuilder();
			cb[i].setOAuthConsumerKey(Helper.KEYS[i][0]);
			cb[i].setOAuthConsumerSecret(Helper.KEYS[i][1]);
			cb[i].setOAuthAccessToken(Helper.KEYS[i][2]);
			cb[i].setOAuthAccessTokenSecret(Helper.KEYS[i][3]);
			twitters[i] = new TwitterFactory(cb[i].build()).getInstance();
		}
		int total = Helper.PLACES_IDS.length;



		try {
			//write to file
			File file = new File("suburb geo info.txt");
			if(!file.exists())file.createNewFile();
			FileWriter fw = new FileWriter(file);

			////update rls information
			Map<String,RateLimitStatus> rls = twitters[currentTwitterIndex].getRateLimitStatus();
			RateLimitStatus placeLimit = rls.get("/geo/id/:place_id");
			int placeRemain = placeLimit.getRemaining();

			//
			for(String[] place:Helper.PLACES_IDS){

				Place p = twitters[currentTwitterIndex].getGeoDetails(place[1]);
				placeRemain --;
				total --;
				System.out.println(total);
				
				fw.write(total + "-" + placeRemain+","+p.getName()+",");
				for(GeoLocation[] geo: p.getBoundingBoxCoordinates()){
					for(GeoLocation geo2: geo){
						fw.write(geo2.toString() + " ");
					}
					fw.write("\n");
				}
				fw.flush();
				
				if(placeRemain <= 2){
					currentTwitterIndex = (currentTwitterIndex + 1) % twitters.length;
					rls =  twitters[currentTwitterIndex].getRateLimitStatus();
					placeLimit = rls.get("/geo/id/:place_id");
					placeRemain = placeLimit.getRemaining();

					if(placeRemain <= 2){
						System.out.printf("Force sleep: %d\n",placeLimit.getSecondsUntilReset());
						Thread.sleep(placeLimit.getSecondsUntilReset()*1000);
					}					
				}
			}
			fw.flush();
			fw.close();
		} catch (TwitterException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
