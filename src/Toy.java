import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import twitter4j.*;
import twitter4j.Query.Unit;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
public class Toy {

	public static void main(String[] args){
		try{
			TwitterFactory factory = new TwitterFactory();
			Twitter twitter = factory.getInstance();
			twitter.setOAuthConsumer(Helper.KEYS[0][0], Helper.KEYS[0][1]);
			AccessToken at = new AccessToken(Helper.KEYS[0][2],Helper.KEYS[0][3]);
			twitter.setOAuthAccessToken(at);

			//////////////////////////////////////////////
			//TEST INFO
			/*
			pln("[TEST]: Twitter object");
			pln(twitter);
			pln("[TEST]: Access Token Object");
			pln(at);
			pln("[TEST]: Rate Limit Status");
			pln(twitter.getRateLimitStatus());
			 */

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//post tweet
			/*
			 Status status = twitter.updateStatus("Test from toy application ;P");	
			 */

			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//getting time line		
			/*
			//List<Status> statuses = twitter.getHomeTimeline();
			List<Status> statuses = twitter.getUserTimeline();

			System.out.println("Showing timeline.");
			for (Status status : statuses) {
				System.out.println(status);

				System.out.printf(
						"(User name: %s)(User id: %d)(User screen name: %s)(User Geo: %s)"
						+ "(Tweet: %s)(Tweet Geo: %s)(Tweet Place: %s)\n",
						status.getUser().getName(),
						status.getUser().getId(),
						status.getUser().getScreenName(),
						status.getUser().getLocation(),
						status.getText(),
						status.getGeoLocation(),
						status.getPlace()					
						);
					//status.getUser().get
				pln();
			}
			 */

			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//search for tweets  (how to query later)
			/* <search date range> *can not return result from 6-9 days
			 * 1. setSince() + setUntil()
			 * 2. since:YYYY-MM-DD until:YYYY-MM-DD
			 * 
			 * <search by language>
			 * 1. 日语+中文+英语=日语
			 * 1.5 日语+中文 = 日语
			 * 2. 中文+英文=日语
			 * 3. 英文 = 英文
			 * 4. setLang()
			 * 5. lang:ja
			 * 
			 * <search terms> 
			 * 1. https://dev.twitter.com/rest/public/search
			 * 
			 * <search by spicific location>  //can only enabled by mobile devices
			 * 1. setGeoCode()
			 * 
			 * <search by place> unable to do this， near is unavalable in API
			 * 1. 
			 * 
			 * <positive negative question>
			 * 1. :) :( ?
			 */
			Query query = new Query("place:117fa33927f3b36d"); //can only search on place at a time
			//query.setSince("2010-04-17");
			//query.setUntil("2016-04-22");
			query.setGeoCode(new GeoLocation(-37.8141,144.9633), 50, Unit.km);  
			//query.setLang("en");
			//query.setCount(100);

			QueryResult result = twitter.search(query);
			System.out.println(twitter.getRateLimitStatus("search"));
			pln("----------========== QUERY TEST =============-------");
			for(Status status:result.getTweets()){
				System.out.println(status);
				System.out.printf(
						"(User name: %s)(User id: %d)(User screen name: %s)(User Geo: %s)"
								+ "(Tweet: %s)(Tweet Geo: %s)(Tweet Place: %s)\n",
								status.getUser().getName(),
								status.getUser().getId(),
								status.getUser().getScreenName(),
								status.getUser().getLocation(),
								status.getText(),
								status.getGeoLocation(),
								status.getPlace()			
								//status.get
						);
				pln();
			}

			/*
			for(Status status:result.getTweets()){
				System.out.println(
						"@" + status.getUser().getScreenName() + 
						": " + status.getText()+ 
						" GEO:" +status.getGeoLocation()+
						" Lang:" +status.getLang()+
						" Place:" + status.getPlace() + 
						" RateLimitStatus:" + status.getRateLimitStatus() + 
						" User Description: "+ status.getUser().getDescription()
						);
			}
			 */

			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//Streaming
			/*
			StatusListener listener = new StatusListener() {
				public void onStatus(Status status) {
					System.out.println(status.getUser().getName() + " : " + status.getText());
				}
				public void onException(Exception ex) {
					ex.printStackTrace();
				}
				public void onTrackLimitationNotice(int arg0) {}
				public void onStallWarning(StallWarning arg0) {}
				public void onScrubGeo(long arg0, long arg1) {}
				public void onDeletionNotice(StatusDeletionNotice arg0){}
			};
			TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
			twitterStream.addListener(listener);
			twitterStream.setOAuthConsumer(KeysAndSecrets.consumerKey, KeysAndSecrets.consumerSecret);
			twitterStream.setOAuthAccessToken(at);
			// sample() method internally creates a thread which manipulates TwitterStream and calls these adequate listener methods continuously.
			twitterStream.sample();
			 */

			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// GEO query
			//parkville	:6b93028d13680bde          
			//preston	:117fa33927f3b36d         	 
			//brunswick	:66abea9ab5900f99 		 
			//
			/*
			GeoQuery gq = new GeoQuery(new GeoLocation(-37.709723, 145.048329));
			//GeoQuery gq = new GeoQuery(new GeoLocation(-37.997431, 145.166232));

			//gq.setAccuracy("500000");  //not sure this works
			gq.maxResults(150);
			pln("================= search places");
			ResponseList<Place> resp = twitter.searchPlaces(gq);
			pln(gq.getMaxResults());
			pln(gq.getAccuracy());
			pln(gq.getGranularity());

			//Map<String,String> places = Helper.getPlacesIDs();
			//
			int counter=0;
			for(Place place:resp){

				//////////bounding box
				
				for(GeoLocation[] geo: place.getBoundingBoxCoordinates()){
					for(GeoLocation geo2: geo){
						System.out.println(geo2);
					}
				}
				/////////////////////contained with
				for(Place p:place.getContainedWithIn()){
					System.out.println(p);
				}
				

			}		
			pln(counter);

			pln("=================== Reverse geocode");
			resp = twitter.reverseGeoCode(gq);
			for(Place place:resp){
				pln(place);
				pln(place.getFullName());
			}
			pln("=================== getSimilarPlaces");
			resp = twitter.getSimilarPlaces(new GeoLocation(-37.8141,144.9633), "Brunswick", null, null);
			for(Place place:resp){
				pln(place);
			}
			 */

			/////////////////
			// Rate limit experiment
			//Query query = new Query("place:117fa33927f3b36d");
			/*
			Map<String ,RateLimitStatus> rateLimitStatus = twitter.getRateLimitStatus();
			for (String endpoint : rateLimitStatus.keySet()) {
				RateLimitStatus status = rateLimitStatus.get(endpoint);
				//if(endpoint.equals("/geo/search")){
				System.out.println("Endpoint: " + endpoint);
				System.out.println(" Limit: " + status.getLimit());
				System.out.println(" Remaining: " + status.getRemaining());
				System.out.println(" ResetTimeInSeconds: " + status.getResetTimeInSeconds());
				System.out.println(" SecondsUntilReset: " + status.getSecondsUntilReset());
				//}				
			}
			 */

		}catch(TwitterException te){
			te.printStackTrace();
		}catch(IllegalStateException ise){
			ise.printStackTrace();
		}
	}

	public static void p(){System.out.println();}
	public static void p(String s){System.out.print(s);}
	public static void p(int s){System.out.print(s);}
	public static void pln(){System.out.println();}
	public static void pln(String s){System.out.println(s);}
	public static void pln(int s){System.out.println(s);}
	public static void pln(Object o){System.out.println(o);}
}
