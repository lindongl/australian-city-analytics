import java.util.List;
import org.ektorp.CouchDbConnector;
import org.ektorp.support.CouchDbRepositorySupport;
import org.ektorp.support.GenerateView;
import org.ektorp.support.View;

public class SofaRepository extends CouchDbRepositorySupport<Sofa> {
	public SofaRepository(CouchDbConnector db) {
		super(Sofa.class, db);
		initStandardDesignDocument();
	}

	@GenerateView
	public List<Sofa> findByColor(String color) {
		return queryView("by_color", color);
	}
	
//	@View( name = "avg_sofa_size", map = "if (doc.color == 'Sofa' ) emit( null, doc._id )}", reduce = "function(doc) {...}")
//	public List<Sofa> findByAA(String color){
//		return queryView("avg_sofa_size",color);
//	}
}
