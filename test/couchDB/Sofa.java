
import java.util.Date;
import java.util.List;

import org.ektorp.support.CouchDbDocument;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Sofa extends CouchDbDocument {

    private String color;
    private int seats;
    private Date dateCreated;
    private List<String> imageURLs;
    
    public void setColor(String s) {
            this.color = s;
    }
    
    public int getSeats() {
            return seats;
    }
    
    public void setSeats(int i) {
            this.seats = i;
    }
    
    public String getColor() {
            return color;
    }
    
    public List<String> getImageURLs() {
            return imageURLs;
    }
    
    public void setImageURLs(List<String> imageURLs) {
            this.imageURLs = imageURLs;
    }
    
    @JsonIgnore
    public int getNumberOfImages() {
            return imageURLs != null ? imageURLs.size() : 0;
    }
    
    @JsonProperty("date_created")
    public Date getDateCreated() {
            return dateCreated;
    }
    
    @JsonProperty("date_created")
    public void setDateCreated(Date dateCreated) {
            this.dateCreated = dateCreated;
    }
}