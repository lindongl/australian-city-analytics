import java.net.MalformedURLException;
import java.util.List;

import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;
import org.junit.Before;
import org.junit.Test;

public class Demo {
	CouchDbInstance dbInstance;
	CouchDbConnector db;

	@Before
	public void setup() throws MalformedURLException {
		HttpClient httpClient = new StdHttpClient.Builder().url(
				"http://localhost:5984").build();
		dbInstance = new StdCouchDbInstance(httpClient);
		db = new StdCouchDbConnector("demo12", dbInstance);
	}

	@Test
	public void testCreateDB() {
		db.createDatabaseIfNotExists();
	}


//	@Test
//	public void testCreateDocument() {
//		Sofa sofa = new Sofa();
//		sofa.setColor("Blue");
//		db.create(sofa);
//	}
//
//	@Test
//	public void testReadDocument() {
//		String id = "b19166933cc2d9e293414f8af300706c";
//		Sofa sofa = db.get(Sofa.class, id);
//		System.out.println("Sofa:"+sofa.getId()+" colour: " + sofa.getColor());
//	}
//
//	@Test
//	public void testUpdateDocument() {
//		String id = "b19166933cc2d9e293414f8af300706c";
//		Sofa sofa = db.get(Sofa.class, id);
//		sofa.setColor("red");
//		db.update(sofa);
//	}
//
//	@Test
//	public void testDeleteDocument() {
//		String id = "b19166933cc2d9e293414f8af300706c";
//		Sofa sofa = db.get(Sofa.class, id);
//		db.delete(sofa);
//	}

	@Test
	public void testRepository() {
		SofaRepository sofaRepository = new SofaRepository(db);
		List<Sofa> sofaList = sofaRepository.findByColor("Blue");
		System.out.println(sofaList.get(0).getColor());
	}

//	@Test
//	public void testInLineView() {
//		CouchDbConnector db = new StdCouchDbConnector("demo12", dbInstance);
//		db.createDatabaseIfNotExists();
//	}
//
//	@Test
//	public void testCLasspathView() {
//		CouchDbConnector db = new StdCouchDbConnector("demo12", dbInstance);
//		db.createDatabaseIfNotExists();
//	}
//
//	@Test
//	public void testAutomaticGeneratedView() {
//		CouchDbConnector db = new StdCouchDbConnector("demo12", dbInstance);
//		db.createDatabaseIfNotExists();
//	}
//
//	@Test
//	public void testFieldNameConflicts() {
//		CouchDbConnector db = new StdCouchDbConnector("demo12", dbInstance);
//		db.createDatabaseIfNotExists();
//	}
//
//	/**
//	 * Repositories based on CouchDbRepositorySupport may also define list, show
//	 * and filter functions through annotations in the repository class.
//	 * 
//	 * They all have the same functionality, it is just the type of function
//	 * that differs:
//	 * 
//	 * @Filter( name = "my_filter" file = "my_filter.js")
//	 * @ListFunction( name = "my_list_function" file = "my_list_function.js")
//	 * @ShowFunction( name = "my_show_function" file = "my_show_function.js")
//	 * @UpdateHandler( name = "my_show_function" file = "my_update_handler.js")
//	 *                 public class MyRepository { ... Multiple functions can be
//	 *                 grouped with the corresponding annotations @Filters,
//	 *                 @Lists, @Shows and @UpdateHandlers.
//	 * 
//	 *                 These annotations behaves in effect as the @View and @Views
//	 *                 annotations described in section 3 of this chapter.
//	 */
//	@Test
//	public void testAdditionalDesignDocumentFunctions() {
//		CouchDbConnector db = new StdCouchDbConnector("demo12", dbInstance);
//		db.createDatabaseIfNotExists();
//	}
}
